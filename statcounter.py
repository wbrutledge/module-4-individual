#command line input to ensure proper file location
from __future__ import division 
import sys, os, math, re, operator


if len(sys.argv) < 2:
	sys.exit("Usage: %s takes an input variable: please specify the location of the file to use." % sys.argv[0])
	
filename = sys.argv[1];

if not os.path.exists(filename):
	sys.exit("Error: File '%s' not found" % sys.argv[1])


playersAvgs = {}

#input the specified file
f = open(filename)
for line in f:
	thisLine = line.rstrip()
	#print thisLine
	regex1 = re.compile(r"[A-Za-z -.,0-9]+( runs)$")
	match = regex1.match(thisLine)
	if match is not None:
	#current line is a player's stats
		#match name
		regex2 = re.compile(r"^([A-Za-z- .,0-9]+)(:? batted)")
		nameMatch = regex2.match(thisLine)
		if nameMatch:
			nameStat = nameMatch.group(1)
			
		#match bats
		regex3 = re.compile(r"(:?[A-Za-z -.,0-9]+)( batted )([0-9]{1,2})")
		batMatch = regex3.match(thisLine)
		if batMatch:
			batStat = int(batMatch.group(3))
		
		#match hits
		regex4 = re.compile(r"(:?[A-Za-z -.,0-9]+)( with )([0-9]{1,2})")
		hitMatch = regex4.match(thisLine)
		if hitMatch:
			hitStat = int(hitMatch.group(3))
			avgStat = 0.000
			if batStat > 0:
				avgStat = (hitStat / batStat)
				avgStatPretty = '%.3f'%(avgStat)
		
		#append to existing data	
		if nameStat in playersAvgs:
			oldBatStat = playersAvgs[nameStat][0]
			oldHitStat = playersAvgs[nameStat][1]
			oldAvgStat = playersAvgs[nameStat][2]
			
			batStat += oldBatStat
			hitStat += oldHitStat
			avgStat = (hitStat / batStat)
			avgStatPretty = '%.3f'%(avgStat)
			
			stats = [batStat, hitStat, avgStatPretty]
			playersAvgs[nameStat] = stats;
			
		#otherwise, add new data for new player
		else:
			stats = [batStat, hitStat, avgStatPretty]
			playersAvgs[nameStat] = stats;

f.close()
#Data has been gathered and totalled. Now tidy up and present

playersTotalAvgs = {}

for player in playersAvgs.iterkeys():
	oldAvgStat = playersAvgs[player][2]
	avgStat = playersAvgs[player][2]
	playersTotalAvgs[avgStat] = player

for player, avgStat in sorted(playersTotalAvgs.iteritems(), reverse=True):
	print "%s: %s" % (avgStat, player)

